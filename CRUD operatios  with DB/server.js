const express = require('express');
const config = require('config');
const mongoose = require('mongoose');
const bodyParder = require('body-parser');
const app = express();

app.use(bodyParder.json());
app.use(bodyParder.urlencoded({extended: true}))

mongoose.connect('mongodb://localhost/Books_db');
const db = mongoose.connection;

const Schema = mongoose.Schema;

const bookDataSchema = new Schema({
    title: {type:String, required:true, unique: true},
    author: {type:String, required:true},
    coverPage: {type:String, default: 'https://image.freepik.com/free-vector/comic-book-cover-page-template-design_1017-15145.jpg'},
    dataOfPublish: {type:Date, required:true}
});

const BookDataModel = mongoose.model('BookCollection', bookDataSchema);



db.on('connected', () => {
    console.log('connected successfully');
})

db.on('error', (err) => {
    console.log('connected failed ******', err);
})

app.get('/books', (req, res, next) => {
  BookDataModel.find({}, (err, result) => {
      if(err) {
          next(err)
      } else {
          res.json(result);
      }
  })
})

app.get('/books/:id', (req, res, next) => {
    BookDataModel.findById(req.params.id, (err, result) => {
        if(err) {
            next(err)
        } else {
            res.json(result);
        }
    })
})

app.post('/books', (req, res, next) => {

    BookDataModel.create(req.body
        , (err, result) => {
            if(err) {
                next(err)
            } else {
                res.send(result)
            }
        })
        
})

app.put('/books/:id', (req, res, next) => {
    BookDataModel.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, result) => {
        if(err) {
            next(err)
        } else {
            res.send(result)
        }
    })
})

app.delete('/books/:id', (req, res, next) => {
    BookDataModel.findByIdAndRemove(req.params.id, (err, result) => {
        if(err) {
            next(err)
        } else {
            res.send(result)
        }
    })
})

app.use((err, req, res, next) => {
    console.log(err)
    res.status(400).json({message: err.message});
    
})
const PORT = config.get('PORT');
app.listen(PORT, () => {
    console.log(`Server is listening ${PORT}`)
})