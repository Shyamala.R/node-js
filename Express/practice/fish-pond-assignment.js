/*
1.Initial Temperature has been taken as input
2.got the current time from new Date()
3.Used setInterval() and clearInterval() to repeat the process 
4.Night Time (i have considered the night time as 8 PM to 6 AM), increased and decreased 
Temperature based on time (used if condition to check that).
5.decreased the temperature when it reaches 40 C and increased when it was 30 C
6.each Time when it increase or decrease ,it will check time condition
7.when it reaches 40 C, ity has to reduce gradually, 
so i have considered that ..0.01 0r 0.005 C will be reduced every 2 mins ******
*/

var initialTemperature = 30;
let date = new Date();
let time = date.getHours();

nightTime = 21;
earlyMorning = 6;


function fishPond() {
    var increaseC =  setInterval(() => {
        if(time < nightTime && time > earlyMorning) {
            initialTemperature = initialTemperature + 0.02;
        } else {
            initialTemperature = initialTemperature + 0.01;
        }
        console.log(`Now the Temperature of the Fish Pond is ${initialTemperature.toFixed(2)} C`);
        if(initialTemperature.toFixed(2) >= 40.00) {
            clearInterval(increaseC);
            var decreaseC = setInterval(() => {
                if(time < nightTime && time > earlyMorning) {
                initialTemperature = initialTemperature - 0.01;
                } else {
                initialTemperature = initialTemperature - 0.005;
                }
                console.log(`Now the Temperature of Fish Pond is ${initialTemperature.toFixed(3)} C`)
                if(initialTemperature.toFixed(2) <= 30.00) {
                    clearInterval(decreaseC)
                    fishPond();
                }
            }, 120000)
        }
       }, 300000)
}


fishPond();




