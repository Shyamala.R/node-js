/*
Pseudocode : 
1.access the file through get call
2.get the file name through query method in express
3.check whether the file which we got is valid
4.if the file is valid, then access it through access method in filesystem module 
4.if u can access, then download it using download method from filesystem module
*/
var express = require('express');
const PORT = 5000;
var app = express();
var fs = require('fs');
const path = __dirname + '/assets/'
const fileFromComputer = __dirname + '/html-files/'

app.get('/getimage', (req, res) => {

    var imagename = req.query.imagename;
    if (imagename && imagename.trim() !== "" && imagename != undefined) {
        fs.access((path + imagename), (err) => {
            if (err) {
                res.send('there is no file that u have asked');
            } else {
                console.log('hey i have accessed the file');
                res.download(path + imagename);
                }
        })
    } else {
        res.send('u didnt send any filename to search');
    }
})

// am just downloading a HTML file from html-files folder

app.get('/getfile', (req, res) => {
    res.download((fileFromComputer + 'subjects.html'));
})


app.listen(PORT, () => {
    console.log(`Server is listening a port ${PORT}`);
})