const express = require('express');

const app = express();

const PORT = 5000;

const collegeData = require('./routes/router');

app.use('/college', collegeData);

app.listen(PORT, () => {
    console.log(`I am listening the port ${PORT}`);
})


