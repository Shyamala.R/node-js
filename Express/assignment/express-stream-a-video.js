const express = require('express');
const app = express();

const path = './subjects.html'


const fs = require('fs');
const PATH = __dirname + '/assets/'

app.get('/getaudio', (req, res) => {
    res.writeHead(200, {'Content-Type': 'audio/mp3'});
    let audioStream = fs.createReadStream( PATH + 'two.mp3');
    audioStream.pipe(res);
})

app.get('/getwavaudio', (req, res) => {
    res.writeHead(200, {'Content-Type': 'audio/wav'});
    let wavAudioStream = fs.createReadStream(PATH + 'WAV_1MG.wav');
    wavAudioStream.pipe(res);
})

// Serve a static file
// app.use(express.static())
app.get('/staticFile', (req, res) => {

})

app.listen(5000, () => {
    console.log('Server started successfully and listening the port 5000');
})