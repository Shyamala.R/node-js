
// Goal: To stream a video file

const express   = require('express');

// to help serve a local video file
const fs        = require('fs');
const PATH      = "./public/assets/videos/";
const server    = express();

// Create an instance of the http server to handle HTTP requests
server.get('/earth', function(request, response){
    // Set a response type of mp4 video for the response
    response.writeHead(200, {'Content-Type': 'video/mp4'});

    // Read the video into a stream
    let videoStream = fs.createReadStream( PATH + 'earth.mp4');

    // Pipe our stream into the response
    videoStream.pipe(response);
});

// Start the server on port 3000
server.listen(3000);
