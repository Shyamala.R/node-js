const winston = require('winston');
const getlogData = require('winston');

const logging = getlogData.createLogger({
    transports: [
        new getlogData.transports.File({
            filename: 'application.log',
            level: 'info',
            format: getlogData.format.json()
        })
    ]
})

module.exports = logging