'use strict'

const express = require('express');
const morgan = require('morgan');
const config = require('config');
const fs = require('fs');
const path = require('path');
const app = express();
// defining a file to write data into
const outputStreamData = fs.createWriteStream('outputstream/outputstreamdata.log', {flags: 'a'})



// morgan middleware function to log requests in a file (bydefault it logs in console but am trying here to log it in a file)
app.use(morgan('short', {stream: outputStreamData}));

// getting port number from config/default.json
const PORT = config.get('PORT');

// handing GET request for route /getData
app.get('/getData', (req, res) => {
    res.send('This is morgan to get request logs');
})

// Starting server here
app.listen(PORT, () => {
    console.log(`server listening ${PORT}`);
})