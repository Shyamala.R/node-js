var human = {
    fname: 'Shyamala',
    lname: 'R',
    age: 26,
    gender: 'F'
};
console.log(human);
var Weeks;
(function (Weeks) {
    Weeks[Weeks["sunday"] = 0] = "sunday";
    Weeks[Weeks["monday"] = 1] = "monday";
    Weeks[Weeks["tuesday"] = 2] = "tuesday";
    Weeks[Weeks["wednesday"] = 3] = "wednesday";
    Weeks[Weeks["thursday"] = 4] = "thursday";
    Weeks[Weeks["friday"] = 5] = "friday";
    Weeks[Weeks["saturday"] = 6] = "saturday";
})(Weeks || (Weeks = {}));
var a = Weeks.friday;
console.log(Weeks[a]);
var Employee = /** @class */ (function () {
    function Employee(a, b) {
        this.a = a;
        this.b = b;
    }
    return Employee;
}());
var c = new Employee('50', '15');
console.log(c);
