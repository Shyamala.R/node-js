interface Person{
    fname: string,
    lname: string,
    age: number,
    gender: string
}

let human: Person = {
    fname: 'Shyamala',
    lname: 'R',
    age: 26,
    gender: 'F'
}

console.log(human);

enum Weeks {sunday, monday, tuesday, wednesday, thursday, friday, saturday}

let a = Weeks.friday
console.log(Weeks[a]);

class Employee {
    constructor(public a: string,public b: string) {
    }
}

let c = new Employee('50','15');

console.log(c);


