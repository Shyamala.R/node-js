'use strict'
const express = require('express');
const config = require('config');
const app = express();


app.get('/', (req, res) => {
    res.json({data: 'Simple get call'});
})

const PORT = config.get('PORT');
app.listen(PORT, () => {
    console.log(`Server listening ${PORT}`);
})