const express = require('express');
const router = express.Router();

// normal GET call
router.get('/ceo', (req, res) => {
    res.send('its about ceo')
})

// named parameters
router.get('/singlemeployee/:employeeId', (req, res) => {
    res.send(`its about employee ${req.params.employeeId}`)
})

// normal POST call with all query parameters
router.post('/employee/add', (req, res) => {
    res.send(`New Joiner has been added to the database successfully and displays query parameters ${JSON.stringify(req.query)}`)
})

// fetching data from request in POST call
router.post('/employee', (req, res) => {
    res.send(`Request body ${JSON.stringify(req.body)}`)
})

// PATCH call
router.patch('/partialupdate/1', (req, res) => {
    res.json({message: 'this is patch call'})
})
// PUT call
router.put('/update/1', (req, res) => {
    res.json({message: 'this is put call'})
})
// DELETE call
router.delete('/delete/1', (req, res) => {
    res.json({message: 'this is delete call'})
})

module.exports = router;