const express = require('express');
const config = require('config');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const aboutCompany = require('./routes/about');
const policiesOfCompany = require('./routes/policies');
const employeelistOfCompany = require('./routes/employeelist');

app.use('/about', aboutCompany);
app.use('/policies', policiesOfCompany);
app.use('/employeelist', employeelistOfCompany);

const PORT = config.get('PORT')
app.listen(PORT, () => {
    console.log(`Server listening ${PORT}`);
})